from scipy.stats import pearsonr as corr
from sklearn.decomposition import PCA
import sklearn.linear_model as lm
from sklearn.preprocessing import StandardScaler
import pandas as pd

X = np.array(train)
y = np.array(target).astype(np.int)

for j in range(X.shape[1]):
	r = corr(X[:,j],y)
	print "Variable: %i\tCorr: %f\tp: %f" % (j,r[0],r[1])

df = pd.DataFrame(X)


scaler = StandardScaler()
X_Scaled = scaler.fit_transform(X[:,0:22])

pca = PCA()

pca.fit(X_Scaled)

cumsum = []
for i, eig in enumerate(pca.explained_variance_ratio_):
	cumsum.append(eig)
	print "Eigenvector: %i\tPercent Explained: %f\t Cumulative Percent: %f" % (i+1,eig,sum(cumsum))


pca = PCA(n_components=19)
X_pca = pca.fit_transform(X_Scaled)

logreg = lm.LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
	fit_intercept=True, intercept_scaling=1.0, 
	class_weight=None, random_state=None)
logreg.fit(X_pca,np.array(target).astype(np.int))
print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(logreg, 
	np.array(train), np.array(target).astype(np.int), cv=20, scoring='roc_auc'))

logreg = lm.LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
	fit_intercept=True, intercept_scaling=1.0, 
	class_weight=None, random_state=None)
logreg.fit(X[:,22:],np.array(target).astype(np.int))
print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(logreg, 
	np.array(train), np.array(target).astype(np.int), cv=20, scoring='roc_auc'))




