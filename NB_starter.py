""" Naive Bayes Starter Code - StumbleUpon Kaggle Competition
bensolucky@gmail.com 
Kaggle: BS Man
"""

import json
import numpy as np
import pandas as pd
from sklearn.naive_bayes import BernoulliNB
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn import cross_validation
from scipy.sparse import coo_matrix, csr_matrix, hstack, vstack

# Set a seed for consistant results
###############################################################################
# Load Data into pandas and Preprocess Features
###############################################################################
X = pd.read_csv('train.tsv', sep="\t", na_values=['?'], index_col=1)
X_test = pd.read_csv('test.tsv', sep="\t", na_values=['?'], index_col=1)
y = X['label']
X = X.drop(['label'], axis=1)
# Combine test and train while we do our preprocessing
X_all = pd.concat([X_test, X])

X_all['boilerplate'] = X_all['boilerplate'].apply(json.loads)

# Replace empty bodies with 'empty' string
extractBody = lambda x: x['body'] if x.has_key('body') and x['body'] is not None else u'empty'
X_all['body'] = X_all['boilerplate'].map(extractBody)

# Replace empty titles with 'empty' string
extractTitle = lambda x: x['title'] if x.has_key('title') and x['title'] is not None else u'empty'
X_all['title'] = X_all['boilerplate'].map(extractTitle)

body_counter = CountVectorizer()
body_counts = body_counter.fit_transform(X_all['body'])

title_counter = CountVectorizer()
title_counts = title_counter.fit_transform(X_all['title'])

#body_counts = hstack([body_counts,title_counts])

tfv = TfidfVectorizer(min_df=3,  max_features=None, strip_accents='unicode',  
      analyzer='word',token_pattern=r'\w{1,}',ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1)
body_tfv = tfv.fit_transform(X_all['body'])

# Re-seperate the test and training rows
bodies = body_counts.tocsr()[len(X_test.index):]
bodies_test = body_counts.tocsr()[:len(X_test.index)]

bodies_tfv = body_tfv[len(X_test.index):]
bodies_tfv_test = body_tfv[:len(X_test.index)]

# Fit a model and predict
model = BernoulliNB()
model.fit(bodies, y)
preds = model.predict_proba(bodies)[:,1]
print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(model, 
	bodies, y, cv=20, scoring='roc_auc'))
pred_df = pd.DataFrame(preds, index=X_test.index, columns=['label'])
pred_df.to_csv('nboutput.csv')
