"""
beating the benchmark @StumbleUpon Evergreen Challenge
__author__ : Abhishek Thakur
"""

# -*- coding: utf-8 -*-
import numpy as np
from sklearn import metrics,preprocessing,cross_validation
from sklearn.feature_extraction.text import TfidfVectorizer
import sklearn.linear_model as lm
import pandas as p
from scipy.sparse import coo_matrix, csr_matrix, vstack, hstack
from sklearn import svm, grid_search
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import roc_curve, auc

loadData = lambda f: np.genfromtxt(open(f,'r'), delimiter=' ')



print "loading data.."
traindata = list(np.array(p.read_table('train.tsv'))[:,2])
testdata = list(np.array(p.read_table('test.tsv'))[:,2])
y = np.array(p.read_table('train.tsv'))[:,-1]

alchemy_train = list(np.array(p.read_table('train.tsv'))[:,3])
alchemy_test = list(np.array(p.read_table('test.tsv'))[:,3])

numeric_train = np.array(p.read_table('train.tsv'))[:,4:-1]
numeric_test = np.array(p.read_table('test.tsv'))[:,4:]

#for now, remove numerics with ?'s for missing values
mask = np.array([False,True,True,True,True,True,True,True,True,True,True,
	True,True,False,True,True,False,True,True,True,True,True])
numeric_train = numeric_train[:,mask].astype(np.float)
numeric_test = numeric_test[:,mask].astype(np.float)
y = y.astype(np.int)


tfv = TfidfVectorizer(min_df=3,  max_features=None, strip_accents='unicode',  
      analyzer='word',token_pattern=r'\w{1,}',ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1)
  
tfv_alchemy = TfidfVectorizer(min_df=3,  max_features=None, strip_accents='unicode',  
      analyzer='word',token_pattern=r'\w{1,}',ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1)


X_all = traindata + testdata
alchemy_all = alchemy_train + alchemy_test

lentrain = len(traindata)

print "Fitting boilerplate pipeline."
tfv.fit(X_all)
print "transforming data"
X_all = tfv.transform(X_all)

#fit and transform in one step
print "Fitting alchemy pipeline."
alchemy_all = tfv_alchemy.fit_transform(alchemy_all)
  
X = X_all[:lentrain]
X_test = X_all[lentrain:]

alchemy_X = alchemy_all[:lentrain]
alchemy_test = alchemy_all[lentrain:]

print "Combining matrices."
X= coo_matrix(X)
alchemy_X = coo_matrix(alchemy_X)
numeric_train = coo_matrix(numeric_train)

X_test = coo_matrix(X_test)
alchemy_test = coo_matrix(alchemy_test)
numeric_test = coo_matrix(numeric_test)

X = hstack([X,alchemy_X,numeric_train])
X_test = hstack([X_test,alchemy_test,numeric_test])


#use Stochastic Gradient Descent
def stochastic_gradient_descent(X,y, scale = False, with_mean = False):
  if scale:
    scaler = StandardScaler(with_mean = with_mean)
    scaler.fit(X)
    X = scaler.transform(X)

  sgd = SGDClassifier(loss = 'log', penalty = 'l2', n_iter = 50,
    shuffle = True, warm_start = True)
  sgd.fit(X,y)
  print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(sgd, 
    X, y, cv=20, scoring='roc_auc'))

stochastic_gradient_descent(numeric_train,y, 
  scale = True, with_mean = True)

stochastic_gradient_descent(X,y)

#use grid search for alphas
#L2 Regularization
sgd_classifier = SGDClassifier(loss = 'log', penalty = 'l2', n_iter = 50,
  shuffle = True, warm_start = True)
sgd_parameters = {'alpha':[x*.00001 for x in range(1,11)]}
sgd_grid = grid_search.GridSearchCV(estimator = sgd_classifier,
  param_grid = sgd_parameters, scoring = 'roc_auc', cv = 5)
sgd_grid.fit(X,y)
print sgd_grid.best_estimator_
print sgd_grid.best_score_
print sgd_grid.grid_scores_

#L1 Regularization
sgd_classifier = SGDClassifier(loss = 'log', penalty = 'l1', n_iter = 50,
  shuffle = True, warm_start = True)
sgd_parameters = {'alpha':[1 + .1 * x for x in range(0,11)]}
sgd_grid = grid_search.GridSearchCV(estimator = sgd_classifier,
  param_grid = sgd_parameters, scoring = 'roc_auc', cv = 5)
sgd_grid.fit(X,y)
print sgd_grid.best_estimator_
print sgd_grid.best_score_
print sgd_grid.grid_scores_

#NB Classifier
nb = BernoulliNB()
nb.fit(X,y)
print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(nb, 
  X, y, cv=20, scoring='roc_auc'))

#Standard logistic regression
logreg = lm.LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
  fit_intercept=True, intercept_scaling=1.0, 
  class_weight=None, random_state=None)
logreg.fit(X,y)
print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(logreg, 
  X,y, cv=20, scoring='roc_auc'))

#use logistic regression output as predictor
#in model with other numeric predictors
pred = logreg.predict_proba(X)[:,1]
numerics = np.hstack((pred.reshape(pred.shape[0],1),X))
classifier = RandomForestClassifier(n_estimators=1000,verbose=2,n_jobs=20,
  min_samples_split=5,random_state=1034324)
classifier.fit(numerics,y)
print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(classifier, 
  numerics,y, cv=20, scoring='roc_auc'))

#Apply ensemble predictions
#log and random forrest ensemble
pred_ensemble = logreg.predict_proba(X_test)[:,1]
pred_numerics = np.hstack((pred_ensemble.reshape(pred_ensemble.shape[0],1),
  numeric_test))
pred_out = classifier.predict_proba(pred_numerics)
testfile = p.read_csv('test.tsv', sep="\t", na_values=['?'], index_col=1)
pred_df = p.DataFrame(pred_out[:,1], index=testfile.index, columns=['label'])
pred_df.to_csv('ensemble.csv')

#Apply ensemble predictions
#log and random forrest ensemble, including alchemy categories
#from randomforest.py
numerics = np.hstack((pred.reshape(pred.shape[0],1),train))
classifier = RandomForestClassifier(n_estimators=1000,verbose=2,n_jobs=20,
  min_samples_split=5,random_state=1034324)
classifier.fit(numerics,y)
print "5 Fold CV Score: ", np.mean(cross_validation.cross_val_score(classifier, 
  numerics,y, cv=5, scoring='roc_auc'))
pred_ensemble = logreg.predict_proba(X_test)[:,1]
pred_numerics = np.hstack((pred_ensemble.reshape(pred_ensemble.shape[0],1),
  valid))
pred_out = classifier.predict_proba(pred_numerics)
testfile = p.read_csv('test.tsv', sep="\t", na_values=['?'], index_col=1)
pred_df = p.DataFrame(pred_out[:,1], index=testfile.index, columns=['label'])
pred_df.to_csv('ensemble2.csv')


#apply predictions
print "training on full data"
sgd = SGDClassifier(loss = 'log', penalty = 'l2')
sgd.fit(X,y)
pred = sgd.predict_proba(X_test)[:,1]
testfile = p.read_csv('test.tsv', sep="\t", na_values=['?'], index_col=1)
pred_df = p.DataFrame(pred, index=testfile.index, columns=['label'])
pred_df.to_csv('svm.csv')
print "submission file created.."

